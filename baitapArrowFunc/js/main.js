const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let loadButtonColor = () => {
  var content = "";
  var contentButton = `<button onclick="changeColor()" class="color-button ${colorList[0]} active"></button>`;
  for (let i = 1; i < colorList.length; i++) {
    contentButton += `<button onclick="changeColor()" class="color-button ${colorList[i]}"></button>`;
    content = contentButton;
  }
  document.getElementById("colorContainer").innerHTML = content;
};

loadButtonColor();

let changeColor = () => {
  var house = document.getElementById("house");
  var header = document.getElementById("colorContainer");
  var btns = header.getElementsByClassName("color-button");
  for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function () {
      var curent = document.getElementsByClassName("active");
      curent[0].classList.remove("active");
      this.className += " active";
      var classList = this.className.split(" ");
      house.classList.value = "house " + classList[1];
    });
  }
};
