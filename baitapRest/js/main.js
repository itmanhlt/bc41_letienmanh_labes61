let tinhDTB = (...scores) => {
  var sum = 0;
  scores.forEach((score) => {
    sum += score;
  });
  return sum / scores.length;
};

let renderKhoi1 = () => {
  let toan = document.getElementById("inpToan").value * 1;
  let ly = document.getElementById("inpLy").value * 1;
  let hoa = document.getElementById("inpHoa").value * 1;
  return (document.getElementById("tbKhoi1").innerHTML = tinhDTB(
    toan,
    ly,
    hoa
  ).toLocaleString());
};

let renderKhoi2 = () => {
  let van = document.getElementById("inpVan").value * 1;
  let su = document.getElementById("inpSu").value * 1;
  let dia = document.getElementById("inpDia").value * 1;
  let engLish = document.getElementById("inpEnglish").value * 1;
  return (document.getElementById("tbKhoi2").innerHTML = tinhDTB(
    van,
    su,
    dia,
    engLish
  ).toLocaleString());
};
