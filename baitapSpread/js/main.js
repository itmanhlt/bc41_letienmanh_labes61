let jumpText = () => {
  let str = document.querySelector(".heading").innerHTML;
  let chars = [...str];
  chars = chars.filter((str) => {
    return /\S/.test(str);
  });
  var contentHTML = "";
  chars.forEach(function (chart) {
    var content = `<span>${chart}</span>`;
    contentHTML += content;
  });
  document.querySelector(".heading").innerHTML = contentHTML;
};
jumpText();
